package com.example.dysisapp.ui.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.dysisapp.R;
import com.example.dysisapp.ui.list.ListActivity;
import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginView {

    @BindView(R.id.editId)
    EditText editId;
    @BindView(R.id.editName)
    EditText editName;
    @BindView(R.id.editBarah)
    EditText editBarah;
    @BindView(R.id.editEmail)
    EditText editEmail;
    @BindView(R.id.editUnifiedNo)
    EditText editUnifiedNo;
    @BindView(R.id.editMobileNo)
    EditText editMobileNo;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    LoginPresenter mLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mLoginPresenter = new LoginPresenterImpl(this, new LoginModelImpl());

        mLoginPresenter.checkLogin();
    }

    @OnClick({R.id.btnLogin})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin: {
                mLoginPresenter.onLoginClick(editId.getText().toString(),
                        editName.getText().toString(),
                        editBarah.getText().toString(),
                        editEmail.getText().toString(),
                        editUnifiedNo.getText().toString(),
                        editMobileNo.getText().toString());

                break;
            }
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String errMsg) {
        hideProgress();
        showSnackBar(errMsg);
    }

    @Override
    public void openListActivity() {
        Intent intent = new Intent(LoginActivity.this, ListActivity.class);
        startActivity(intent);
        finish();
    }

    private void showSnackBar(String message) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
                message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        sbView.setBackgroundColor(ContextCompat.getColor(this, R.color.red));
        TextView textView = (TextView) sbView
                .findViewById(R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(this, R.color.white));
        snackbar.show();
    }
}
