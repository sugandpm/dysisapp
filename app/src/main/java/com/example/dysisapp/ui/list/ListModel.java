package com.example.dysisapp.ui.list;

public interface ListModel {
    void getNewsFromServer(ListModelImpl.OnCallbackListener listener);
}
