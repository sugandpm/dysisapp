package com.example.dysisapp.ui.login;

import android.util.Log;

import com.example.dysisapp.dataModel.login.Login;
import com.example.dysisapp.util.PreferencesManager;

public class LoginPresenterImpl implements LoginPresenter, LoginModelImpl.OnCallbackListener {

    private LoginView mLoginView;
    private LoginModel mLoginModel;

    final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    String eId, name, idBarahNo, email, unifiedNo, mobileNo;


    public LoginPresenterImpl(LoginView loginView, LoginModel loginModel) {
        mLoginView = loginView;
        mLoginModel = loginModel;
    }

    @Override
    public void onLoginClick(String eId, String name, String idBarahNo, String email, String unifiedNo, String mobileNo) {

        if (mLoginView != null) {
            mLoginView.showProgress();
        }

        if (eId == null || eId.isEmpty()) {
            mLoginView.showError("Please provide id");
            return;
        }
        if (name == null || name.isEmpty()) {
            mLoginView.showError("Please provide name");
            return;
        }
        if (idBarahNo == null || idBarahNo.isEmpty()) {
            mLoginView.showError("Please provide id barah number");
            return;
        }
        if (email == null || email.isEmpty()) {
            mLoginView.showError("Please provide email address");
            return;
        }
        if (unifiedNo == null || unifiedNo.isEmpty()) {
            mLoginView.showError("Please provide unified number");
            return;
        }
        if (mobileNo == null || mobileNo.isEmpty()) {
            mLoginView.showError("Please provide mobile number");
            return;
        }
        if (!email.matches(EMAIL_PATTERN)) {
            mLoginView.showError("Please provide valid email address");
            return;
        }

        this.eId = eId;
        this.name = name;
        this.idBarahNo = idBarahNo;
        this.email = email;
        this.unifiedNo = unifiedNo;
        this.mobileNo = mobileNo;

        mLoginModel.postLoginDetail(this, eId, name, idBarahNo, email, unifiedNo, mobileNo);


    }

    @Override
    public void checkLogin() {
        if(!PreferencesManager.getInstance().getSingleValue(PreferencesManager.REFERENCE_NUMBER).isEmpty()){
            mLoginView.openListActivity();
        }
    }

    @Override
    public void onFailed(String msg) {
        mLoginView.showError(msg);

    }

    @Override
    public void onSuccess(Login login) {
        if(login.getSuccess()){
            PreferencesManager.getInstance().setValues(eId, name, idBarahNo, email,unifiedNo, mobileNo,
                    login.getPayload().getReferenceNo().toString());

            Log.d("Login", login.getPayload().getReferenceNo().toString());

            mLoginView.hideProgress();
            mLoginView.openListActivity();

        }else{
            mLoginView.showError(login.getMessage());
        }

    }
}
