package com.example.dysisapp.ui.list;

import com.example.dysisapp.dataModel.list.News;
import com.example.dysisapp.util.PreferencesManager;

public class ListPresenterImpl implements ListPresenter, ListModelImpl.OnCallbackListener {

    private ListView mListView;
    private ListModel mListModel;

    public ListPresenterImpl(ListView listView, ListModel listModel) {
        mListView = listView;
        mListModel = listModel;
    }

    @Override
    public void getNews() {
        mListModel.getNewsFromServer(this);
    }

    @Override
    public void logout() {
        PreferencesManager.getInstance().clear();
        mListView.goBackLogin();
    }

    @Override
    public void onSuccess(News news) {
        mListView.showList(news);
    }

    @Override
    public void onFailed(String string) {

    }
}
