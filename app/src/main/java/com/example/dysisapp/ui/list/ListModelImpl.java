package com.example.dysisapp.ui.list;

import com.example.dysisapp.dataModel.list.News;
import com.example.dysisapp.util.HttpManager;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ListModelImpl implements ListModel {


    OnCallbackListener mListener;


    private Observable<News> getNewsObservable() {
        return HttpManager
                .getInstance()
                .getService()
                .getNews()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void getNewsFromServer(OnCallbackListener listener) {
        mListener = listener;
        getNewsObservable().subscribe(new NewsObserver());
    }

    private class NewsObserver implements Observer<News> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            mListener.onFailed(e.getMessage());

        }

        @Override
        public void onNext(final News news ) {
            mListener.onSuccess(news);
        }

    }


    public interface OnCallbackListener {
        void onSuccess(News news);
        void onFailed(String string);

    }
}
