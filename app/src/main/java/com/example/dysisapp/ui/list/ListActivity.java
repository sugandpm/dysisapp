package com.example.dysisapp.ui.list;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.dysisapp.R;
import com.example.dysisapp.dataModel.list.News;
import com.example.dysisapp.ui.login.LoginActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListActivity extends AppCompatActivity implements ListView {

    @BindView((R.id.swipeRefreshLayout))
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recylerView)
    RecyclerView recyclerView;

    ListPresenter mListPresenter;
    NewsAdapter newsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);

        mListPresenter = new ListPresenterImpl(this, new ListModelImpl());


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // Fetching data from server
                mListPresenter.getNews();
            }
        });

        swipeRefreshLayout.post(new Runnable() {

            @Override
            public void run() {

                swipeRefreshLayout.setRefreshing(true);

                // Fetching data from server
                mListPresenter.getNews();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.logout:
                showLogoutAlert();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showList(News news) {

        newsAdapter=new NewsAdapter(ListActivity.this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        newsAdapter.setDao(news.getPayload());
        recyclerView.setAdapter(newsAdapter);

        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void goBackLogin() {
        Intent intent = new Intent(ListActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void showLogoutAlert() {
        AlertDialog alertVerificationDialog = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = LayoutInflater.from(this);
        View dialogView = inflater.inflate(R.layout.dialog_logout, null);
        alertVerificationDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertVerificationDialog.setView(dialogView);

        dialogView.findViewById(R.id.btnLogout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListPresenter.logout();
            }
        });
        dialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                alertVerificationDialog.dismiss();
            }
        });

        alertVerificationDialog.setCancelable(false);
        alertVerificationDialog.show();
    }

}
