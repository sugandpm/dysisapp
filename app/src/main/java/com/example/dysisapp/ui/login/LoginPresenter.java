package com.example.dysisapp.ui.login;

public interface LoginPresenter {
    void onLoginClick(String eId, String name, String idBarahNo, String email, String unifiedNo,
                      String mobileNo);

    void checkLogin();
}
