package com.example.dysisapp.ui.list;

public interface ListPresenter {
    void getNews();

    void logout();
}
