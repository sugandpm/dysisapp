package com.example.dysisapp.ui.list;

import com.example.dysisapp.dataModel.list.News;

public interface ListView {
    void showList(News news);

    void goBackLogin();
}
