package com.example.dysisapp.ui.login;

public interface LoginView {
    void showProgress();

    void hideProgress();

    void showError(String err);

    void openListActivity();
}
