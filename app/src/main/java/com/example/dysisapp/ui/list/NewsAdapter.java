package com.example.dysisapp.ui.list;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dysisapp.R;
import com.example.dysisapp.dataModel.list.Payload;
import com.example.dysisapp.util.ImageDownloaderTask;

import java.util.List;
import java.util.concurrent.RejectedExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    ItemClickListener itemClickListener;
    List<Payload> mPayloads;

    public NewsAdapter(Context context) {

        this.mContext = context;
    }

    public void setDao(List<Payload> dao) {

        this.mPayloads = dao;
    }

    public interface ItemClickListener {

        public void onItemClick(String filterCategoryName);
    }

    public void setOnItemClickListener(ItemClickListener listener){
        itemClickListener = listener;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

            View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_list, viewGroup, false);
            return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder myViewHolder, int i) {

            MyViewHolder itemViewHolder = (MyViewHolder) myViewHolder;

            itemViewHolder.textTitle.setText(mPayloads.get(i).getTitle());
            itemViewHolder.textDate.setText(mPayloads.get(i).getDate());

//            new ImageDownloaderTask(itemViewHolder.iv, mContext).execute(mPayloads.get(i).getImage());

        try {

            new ImageDownloaderTask(itemViewHolder.iv, mContext).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mPayloads.get(i).getImage());

        } catch (RejectedExecutionException e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {

        return mPayloads.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.iv)
        ImageView iv;
        @BindView(R.id.textTitle)
        TextView textTitle;
        @BindView(R.id.textDate)
        TextView textDate;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
        }
    }

}

