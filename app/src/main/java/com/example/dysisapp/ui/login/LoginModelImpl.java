package com.example.dysisapp.ui.login;

import com.example.dysisapp.dataModel.login.Login;
import com.example.dysisapp.util.HttpManager;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginModelImpl implements LoginModel {

    OnCallbackListener mListener;

    @Override
    public void postLoginDetail(OnCallbackListener listener, String eId, String name, String idBarahNo, String email, String unifiedNo,
                                String mobileNo) {

        mListener = listener;
        getLoginObservable(eId, name, idBarahNo, email, unifiedNo, mobileNo).subscribe(new LoginObserver());
    }

    private Observable<Login> getLoginObservable(String eId, String name, String idBarahNo, String email,
                                                 String unifiedNo, String mobileNo) {
        return HttpManager
                .getInstance()
                .getService()
                .postLogin(eId, name, idBarahNo, email, unifiedNo, mobileNo)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private class LoginObserver implements Observer<Login> {

        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            mListener.onFailed(e.getMessage());

        }

        @Override
        public void onNext(final Login login ) {
            mListener.onSuccess(login);
        }

    }


    public interface OnCallbackListener {
        void onSuccess(Login login);
        void onFailed(String string);

    }

}
