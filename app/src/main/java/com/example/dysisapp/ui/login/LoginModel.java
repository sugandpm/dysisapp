package com.example.dysisapp.ui.login;

public interface LoginModel {

    void postLoginDetail(LoginModelImpl.OnCallbackListener listener, String eId, String name, String idBarahNo,
                         String email, String unifiedNo, String mobileNo);

}
