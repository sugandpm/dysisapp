package com.example.dysisapp;

import android.app.Application;

import com.example.dysisapp.util.Contextor;

public class DysisApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Contextor.getInstance().init(getApplicationContext());
    }
}