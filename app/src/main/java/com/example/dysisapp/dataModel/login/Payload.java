
package com.example.dysisapp.dataModel.login;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payload implements Parcelable
{

    @SerializedName("referenceNo")
    @Expose
    private Integer referenceNo;
    public final static Creator<Payload> CREATOR = new Creator<Payload>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Payload createFromParcel(Parcel in) {
            return new Payload(in);
        }

        public Payload[] newArray(int size) {
            return (new Payload[size]);
        }

    }
    ;

    protected Payload(Parcel in) {
        this.referenceNo = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public Payload() {
    }

    public Integer getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(Integer referenceNo) {
        this.referenceNo = referenceNo;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(referenceNo);
    }

    public int describeContents() {
        return  0;
    }

}
