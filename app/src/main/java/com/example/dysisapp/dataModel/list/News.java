
package com.example.dysisapp.dataModel.list;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class News implements Parcelable
{

    @SerializedName("payload")
    @Expose
    private List<Payload> payload = null;
    public final static Creator<News> CREATOR = new Creator<News>() {


        @SuppressWarnings({
            "unchecked"
        })
        public News createFromParcel(Parcel in) {
            return new News(in);
        }

        public News[] newArray(int size) {
            return (new News[size]);
        }

    }
    ;

    protected News(Parcel in) {
        in.readList(this.payload, (Payload.class.getClassLoader()));
    }

    public News() {
    }

    public List<Payload> getPayload() {
        return payload;
    }

    public void setPayload(List<Payload> payload) {
        this.payload = payload;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(payload);
    }

    public int describeContents() {
        return  0;
    }

}
