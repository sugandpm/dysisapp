package com.example.dysisapp.util;

import com.example.dysisapp.dataModel.list.News;
import com.example.dysisapp.dataModel.login.Login;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

public interface ApiService {

    @FormUrlEncoded
    @POST("iskan/v1/certificates/towhomitmayconcern")
    Observable<Login> postLogin(@Field("eid") String eId, @Field("name") String name,
                                       @Field("idbarahno") String idBarahNo, @Field("emailaddress") String email,
                                       @Field("unifiednumber") String unifiedNumber, @Field("mobileno") String mobileNo);

    @GET("public/v1/news?local=en")
    Observable<News> getNews();


}
