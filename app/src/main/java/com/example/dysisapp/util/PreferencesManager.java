package com.example.dysisapp.util;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesManager {

    private static final String PREF_NAME = "dysis_data";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String ID_BARAH_NUMBER = "id_barah_no";
    public static final String UNIFIED_NUMBER = "unified_no";
    public static final String MOBILE_NUMBER = "mobile_no";
    public static final String EMAIL = "email";
    public static final String REFERENCE_NUMBER = "referenceNo";

    private static PreferencesManager instance;
    private final SharedPreferences prefs;
    private Context mContext;

    private PreferencesManager() {
        mContext = Contextor.getInstance().getContext();
        prefs = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static PreferencesManager getInstance() {
        if (instance == null) {
            instance = new PreferencesManager();
        }
        return instance;
    }

    public void setValues(String eId, String name, String idBarahNo, String email, String unifiedNo, String mobileNo, String ref) {
        prefs.edit()
                .putString(ID, eId)
                .putString(NAME, name)
                .putString(ID_BARAH_NUMBER, idBarahNo)
                .putString(EMAIL, email)
                .putString(UNIFIED_NUMBER, unifiedNo)
                .putString(MOBILE_NUMBER, mobileNo)
                .putString(REFERENCE_NUMBER, ref)
                .commit();
    }

    public String getSingleValue(String key) {
        return prefs.getString(key, "");
    }

    public void remove(String key) {
        prefs.edit()
                .remove(key)
                .commit();
    }

    public boolean clear() {
        return prefs.edit()
                .clear()
                .commit();
    }

}