package com.example.dysisapp.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.example.dysisapp.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class ImageDownloaderTask extends AsyncTask<String, Void, Bitmap> {
    ImageView imageView;
    Context context;

    public ImageDownloaderTask(ImageView imageView, Context context) {
        this.imageView = imageView;
        this.context = context;
    }

    @Override
    protected Bitmap doInBackground(String... params) {

//        URL url = null;
//        byte[] bytes = null;
//        HttpURLConnection connection=null;
//        try {
//            url = new URL(params[0]);
//            connection=(HttpURLConnection) url.openConnection();
//            connection.setRequestProperty("Connection", "close");
//            connection.setRequestMethod("GET");
//            connection.setUseCaches(true);
//            InputStream is = null;
//            is=connection.getInputStream();
//            bytes = readBytes(is);
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (ProtocolException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        if (connection!=null)
//            connection.disconnect();
//        Bitmap res=null;
//        if(!isCancelled() && bytes!=null)
//            res=BitmapFactory.decodeByteArray(bytes,0,bytes.length);
//        return res;

        for (String urlStr : params){
            try {
                URL url = new URL(urlStr);
                HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.setUseCaches(true);
                connection.connect();
                InputStream inputStream = connection.getInputStream();
                Bitmap bMap = BitmapFactory.decodeStream(inputStream);
                return bMap;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;

//        return downloadBitmap(params[0]);
    }

    public byte[] readBytes(InputStream inputStream) throws IOException {
        // this dynamically extends to take the bytes you read
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        // we need to know how may bytes were read to write them to the byteBuffer
        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }



    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (isCancelled()) {
            bitmap = null;
        }

//        if (imageViewReference != null) {
//            ImageView imageView = imageViewReference.get();
//            if (imageView != null) {
//                if (bitmap != null) {
//                    imageView.setImageBitmap(bitmap);
//                } else {
//                    Drawable placeholder = imageView.getContext().getDrawable(R.mipmap.ic_launcher);
//                    imageView.setImageDrawable(placeholder);
//                }
//            }
//        }
        imageView.setImageBitmap(bitmap);

    }

    private Bitmap downloadBitmap(String urlString) {
        Bitmap bitmap = null;
        InputStream in = null;
        int responseCode = -1;

        URL url = null;
        try {
            url = new URL(urlString);

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.connect();
            responseCode = httpURLConnection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                in = httpURLConnection.getInputStream();
                bitmap = BitmapFactory.decodeStream(in);
                in.close();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }
}